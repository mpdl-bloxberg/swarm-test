#!/bin/bash 

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner


sudo gitlab-runner register -n \
	--url https://gitlab.mpcdf.mpg.de \
	--token $TOKEN \
	--executor "shell" \
	--description "shell-runner"

sudo usermod -aG docker gitlab-runner

sudo gitlab-runner run &>/dev/null &
disown
