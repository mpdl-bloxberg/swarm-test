#!/bin/bash

# register runner writes config into /srv/gitlab-runner
docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner \
	gitlab/gitlab-runner  \
	register \
	--non-interactive \
	--url https://gitlab.mpcdf.mpg.de \
	--token $TOKEN \
	--executor "docker" \
	--docker-image alpine:latest \
	--docker-image "docker:24.0.5" \
	--docker-privileged \
	--docker-volumes "/certs/client" \
	--description "docker-runner"

# starts runner with the config genereated in teh previous step 
docker run -d -v /srv/gitlab-runner/config:/etc/gitlab-runner \
	-v /var/run/docker.sock:/var/run/docker.sock \
	gitlab/gitlab-runner 
