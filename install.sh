#!/bin/bash 

docker network create -d overlay --attachable traefik
docker stack deploy -d -c docker-compose.yml traefik
